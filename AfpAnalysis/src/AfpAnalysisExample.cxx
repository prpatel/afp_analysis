
#include <AfpAnalysis/AfpAnalysisExample.h>

#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODForward/AFPSiHitContainer.h>
#include <xAODForward/AFPSiHit.h>
#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>
#include <AfpAnalysisTools/AfpAnalysisTool.h>
#include <PATInterfaces/SystematicsUtil.h>
#include <xAODCore/ShallowCopy.h>
#include <TH1.h>
#include <utility>
#include <iostream>
#include <cmath>
#include <fstream>
#include <TTree.h>
#include <TSystem.h>
#include <functional>
#include <algorithm>


AfpAnalysisExample ::AfpAnalysisExample(const std::string &name, ISvcLocator *pSvcLocator) : EL::AnaAlgorithm(name, pSvcLocator),
                                                                                             m_afpTool("AFP::AfpAnalysisTool/afpTool", this),
                                                                                             m_trigConfigTool("TrigConf::xAODConfigTool/xAODConfigTool"),
                                                                                             m_trigDecisionTool("Trig::TrigDecisionTool/TrigDecisionTool"),
                                                                                              m_jetCalibrationTool("JetCalibrationTool", this)
                                                                                              
                                                                                          
                                                                                             //jet_event_file("/eos/user/p/prpatel/afp_analysis/run/all_aod_event.txt", std::ios::app)
// current_pos(0)
//  aod_event_file("/eos/user/p/prpatel/afp_analysis/run/aod_event.txt")
{
  m_afpTool.declarePropertyFor(this, "afpTool", "Tool providing AFP information");
  m_afpTool.setTypeAndName("AFP::AfpAnalysisTool/afpTool");
  declareProperty("Trigger", m_Trigger = "HLT_j20_L1AFP_A_OR_C", "Trigger");
  declareProperty ("jetCalibrationTool", m_jetCalibrationTool, "the jet calibration tool");
  asg::AnaToolHandle<IJetCalibrationTool> m_jetCalibration("JetCalibrationTool/JetCalibrationTool", this);
}

StatusCode AfpAnalysisExample ::initialize()
{
  ANA_CHECK(m_afpTool.retrieve());

  ANA_MSG_INFO(m_afpTool->configInfo());

  // Initialize the TrigDecisionTool
  ANA_CHECK(m_trigConfigTool.initialize());
  ANA_CHECK(m_trigDecisionTool.setProperty("ConfigTool", m_trigConfigTool.getHandle()));
  ANA_CHECK(m_trigDecisionTool.setProperty("TrigDecisionKey", "xTrigDecision"));
  ANA_CHECK(m_trigDecisionTool.initialize());
  //create an object to store the event number
  //ANA_CHECK(book(TH1F("event_number", "event_number", 1, 0, -1)));

  xAOD::TEvent *event = wk()->xaodEvent();
  TFile *file = wk()->getOutputFile("skim");
  ANA_CHECK(event->writeTo(file));


  //std::string file_name = wk()->inputFileName();
  //std::cout << "file_name = " << file_name << std::endl;

  ANA_CHECK(m_jetCalibrationTool.retrieve().isSuccess());

  ANA_CHECK(book(TH1F("lead_jet_pt_before_Calib", "lead_jet_pt_before_Calib", 100, 0, 100)));
  ANA_CHECK(book(TH1F("sub_lead_jet_pt_before_Calib", "sub_lead_jet_pt_before_Calib", 100, 0, 100)));
  ANA_CHECK(book(TH1F("lead_jet_pt_after_Calib", "lead_jet_pt_after_Calib", 100, 0, 100)));
  ANA_CHECK(book(TH1F("sub_lead_jet_pt_after_Calib", "sub_lead_jet_pt_after_Calib", 100, 0, 100)));

  //2d histograms to compare relative pt of jets before and after calibration
  ANA_CHECK(book(TH2F("lead_jet_pt_before_Calib_vs_after_Calib", "lead_jet_pt_before_Calib_vs_after_Calib", 100, 0, 100, 100, 0, 100)));
  ANA_CHECK(book(TH2F("sub_lead_jet_pt_before_Calib_vs_after_Calib", "sub_lead_jet_pt_before_Calib_vs_after_Calib", 100, 0, 100, 100, 0, 100)));

  ANA_CHECK(book(TH1F("lead_jet_eta_before_Calib", "lead_jet_eta_before_Calib", 100, -5, 5)));
  ANA_CHECK(book(TH1F("sub_lead_jet_eta_before_Calib", "sub_lead_jet_eta_before_Calib", 100, -5, 5)));
  ANA_CHECK(book(TH1F("lead_jet_eta_after_Calib", "lead_jet_eta_after_Calib", 100, -5, 5)));
  ANA_CHECK(book(TH1F("sub_lead_jet_eta_after_Calib", "sub_lead_jet_eta_after_Calib", 100, -5, 5)));

  //2d histograms to compare relative eta of jets before and after calibration
  ANA_CHECK(book(TH2F("lead_jet_eta_before_Calib_vs_after_Calib", "lead_jet_eta_before_Calib_vs_after_Calib", 100, -5, 5, 100, -5, 5)));
  ANA_CHECK(book(TH2F("sub_lead_jet_eta_before_Calib_vs_after_Calib", "sub_lead_jet_eta_before_Calib_vs_after_Calib", 100, -5, 5, 100, -5, 5)));

  ANA_CHECK(book(TH1F("lead_jet_phi_before_Calib", "lead_jet_phi_before_Calib", 100, -5, 5)));
  ANA_CHECK(book(TH1F("sub_lead_jet_phi_before_Calib", "sub_lead_jet_phi_before_Calib", 100, -5, 5)));
  ANA_CHECK(book(TH1F("lead_jet_phi_after_Calib", "lead_jet_phi_after_Calib", 100, -5, 5)));
  ANA_CHECK(book(TH1F("sub_lead_jet_phi_after_Calib", "sub_lead_jet_phi_after_Calib", 100, -5, 5)));

  //2d histograms to compare relative phi of jets before and after calibration
  ANA_CHECK(book(TH2F("lead_jet_phi_before_Calib_vs_after_Calib", "lead_jet_phi_before_Calib_vs_after_Calib", 100, -5, 5, 100, -5, 5)));
  ANA_CHECK(book(TH2F("sub_lead_jet_phi_before_Calib_vs_after_Calib", "sub_lead_jet_phi_before_Calib_vs_after_Calib", 100, -5, 5, 100, -5, 5)));
  
  //2d histogram for comparing top 10 jet index before and after calibration
  ANA_CHECK(book(TH2F("top10Jetsindex_before_Calib_vs_after_Calib", "top10Jetsindex_before_Calib_vs_after_Calib", 10, -0.5, 9.5, 10, -0.5, 9.5)));

  ANA_CHECK(book(TH2F("JVT_vs_pt_before_Calib", "JVT_vs_pt_before_Calib", 100, 0, 100, 100, -1, 2)));
  ANA_CHECK(book(TH2F("JVT_vs_pt_after_Calib", "JVT_vs_pt_after_Calib", 100, 0, 100, 100, -1, 2)));

  ANA_CHECK(book(TH2F("Timing_vs_pt_before_Calib", "Timing_vs_pt_before_Calib", 100, 0, 100, 100, -50, 50)));
  ANA_CHECK(book(TH2F("Timing_vs_pt_after_Calib", "Timing_vs_pt_after_Calib", 100, 0, 100, 100, -50, 50)));


  return StatusCode::SUCCESS;
}

StatusCode AfpAnalysisExample ::execute()
{
  xAOD::TEvent *event = wk()->xaodEvent();
  const xAOD::EventInfo *eventInfo = 0;
  ANA_CHECK(event->retrieve(eventInfo, "EventInfo"));


    if((eventInfo->lumiBlock() >= 1 && eventInfo->lumiBlock() <= 27)
    || (eventInfo->lumiBlock() >= 206 && eventInfo->lumiBlock() <= 325)){

      // allJets
      const xAOD::JetContainer *allJets = 0;
      ANA_CHECK(event->retrieve(allJets, "AntiKt4EMPFlowJets"));


      // find the two leading jets
      const xAOD::Jet *leadJet = 0;
      const xAOD::Jet *subLeadJet = 0;

      int lead_jet_index = 0;
      int sub_lead_jet_index = 0;

      for (const xAOD::Jet *jet : *allJets)
      {

        if (!leadJet || jet->pt() > leadJet->pt())
        {
          subLeadJet = leadJet;
          sub_lead_jet_index = lead_jet_index;
          leadJet = jet;
          lead_jet_index = jet->index();
        }
        else if (!subLeadJet || jet->pt() > subLeadJet->pt())
        {
          subLeadJet = jet;
          sub_lead_jet_index = jet->index();
        }
        if(allJets->size() > 1)
        {
          hist("JVT_vs_pt_before_Calib")->Fill(jet->pt() / 1000., jet->auxdata<float>("Jvt"));
          hist("Timing_vs_pt_before_Calib")->Fill(jet->pt() / 1000., jet->auxdata<float>("Timing"));
        }

      }
    

      //sort the allJets by pt but keep the original index
      std::vector<std::pair<int, const xAOD::Jet *>> sortedJets;
      for (const xAOD::Jet *jet : *allJets)
      {
        sortedJets.push_back(std::make_pair(jet->index(), jet));
      }

      std::sort(sortedJets.begin(), sortedJets.end(), [](const std::pair<int, const xAOD::Jet *> &a, const std::pair<int, const xAOD::Jet *> &b) { return a.second->pt() > b.second->pt(); });

      //store the index of the top 10 sorted jets
      std::vector<int> top10JetsIndex;
      std::vector<int> rank_before_Calib;
      
      if(sortedJets.size() > 1 && sortedJets.size() <= 10)
        for (size_t i = 0 ; i < sortedJets.size(); i++)
        {
          //std::cout<<"sortedJets["<<i<<"] = "<<sortedJets[i].first<<std::endl;
          top10JetsIndex.push_back(sortedJets[i].first);
          rank_before_Calib.push_back(i);
          
        }

        

      //fill histograms
      if (leadJet && subLeadJet)
      {
        hist("lead_jet_pt_before_Calib")->Fill(leadJet->pt() / 1000.);
        hist("sub_lead_jet_pt_before_Calib")->Fill(subLeadJet->pt() / 1000.);
        hist("lead_jet_eta_before_Calib")->Fill(leadJet->eta());
        hist("sub_lead_jet_eta_before_Calib")->Fill(subLeadJet->eta());
        hist("lead_jet_phi_before_Calib")->Fill(leadJet->phi());
        hist("sub_lead_jet_phi_before_Calib")->Fill(subLeadJet->phi());
      }

      //apply calibration
      auto shallowCopy = xAOD::shallowCopyContainer(*allJets);
      std::unique_ptr<xAOD::JetContainer> shallowJets(shallowCopy.first);
      std::unique_ptr<xAOD::ShallowAuxContainer> shallowAuxJets(shallowCopy.second);
      ANA_CHECK (m_jetCalibrationTool->applyCalibration(*shallowJets).isSuccess());
      //save modified jets

      ANA_CHECK (event->record(shallowJets.release(), "CalibAntiKt4EMPFlowJets"));
      ANA_CHECK (event->record(shallowAuxJets.release(), "CalibAntiKt4EMPFlowJetsAux."));

      
      const xAOD::JetContainer *calibJets = 0;
      ANA_CHECK(event->retrieve(calibJets, "CalibAntiKt4EMPFlowJets"));

      const xAOD::Jet *calibLeadJet = 0;
      const xAOD::Jet *calibSubLeadJet = 0;

      for (const xAOD::Jet *jet : *calibJets)
      {
        if (static_cast<int>(jet->index()) == lead_jet_index)
        {
          calibLeadJet = jet;
          
        }
        else if (static_cast<int>(jet->index()) == sub_lead_jet_index)
        {
          calibSubLeadJet = jet;
          //std::cout<<"calibSubLeadJet->index() = "<<calibSubLeadJet->index()<<std::endl;
        }
        if(calibJets->size() > 1)
        {
          hist("JVT_vs_pt_after_Calib")->Fill(jet->pt() / 1000., jet->auxdata<float>("Jvt"));
          hist("Timing_vs_pt_after_Calib")->Fill(jet->pt() / 1000., jet->auxdata<float>("Timing"));
        }
       
      }


      //sort calibJets by pt and store the index
      std::vector<std::pair<int, const xAOD::Jet *>> sortedCalibJets;
      for (const xAOD::Jet *jet : *calibJets)
      {
        sortedCalibJets.push_back(std::make_pair(jet->index(), jet));
      }
      std::sort(sortedCalibJets.begin(), sortedCalibJets.end(), [](const std::pair<int, const xAOD::Jet *> &a, const std::pair<int, const xAOD::Jet *> &b) { return a.second->pt() > b.second->pt(); });

      // find the rank of the sortedCalibJets if index of the jet is top10JetsIndex
      std::vector<int> top10JetsIndexCalib;
    

      if(sortedCalibJets.size() > 1 && sortedCalibJets.size() <= 10)
        for (size_t i = 0 ; i < sortedCalibJets.size(); i++)
        {
          //std::cout<<"sortedCalibJets["<<i<<"] = "<<sortedCalibJets[i].first<<std::endl;
          for(size_t j = 0; j < top10JetsIndex.size(); j++)
          {
            if(sortedCalibJets[i].first == top10JetsIndex[j])
            {
              top10JetsIndexCalib.push_back(i);
              //std::cout<<"index = "<< top10JetsIndex[j] << "rank = "<<i<<std::endl;
            }
          }
        }
      
      for (size_t i = 0; i < top10JetsIndex.size(); i++)
      {
        hist("top10Jetsindex_before_Calib_vs_after_Calib")->Fill(rank_before_Calib[i], top10JetsIndexCalib[i]);
        if(rank_before_Calib[i] != top10JetsIndexCalib[i])
        {
          std::cout<<"rank_before_Calib["<<i<<"] = "<<rank_before_Calib[i]<<std::endl;
          std::cout<<"top10JetsIndexCalib["<<i<<"] = "<<top10JetsIndexCalib[i]<<std::endl;
        }
      }
      

      if (calibLeadJet && calibSubLeadJet)
      {
        hist("lead_jet_pt_after_Calib")->Fill(calibLeadJet->pt() / 1000.);
        hist("sub_lead_jet_pt_after_Calib")->Fill(calibSubLeadJet->pt() / 1000.);
        hist("lead_jet_eta_after_Calib")->Fill(calibLeadJet->eta());
        hist("sub_lead_jet_eta_after_Calib")->Fill(calibSubLeadJet->eta());
        hist("lead_jet_phi_after_Calib")->Fill(calibLeadJet->phi());
        hist("sub_lead_jet_phi_after_Calib")->Fill(calibSubLeadJet->phi());
      }

      
      if (leadJet && calibLeadJet)
      {
        hist("lead_jet_pt_before_Calib_vs_after_Calib")->Fill(leadJet->pt() / 1000., calibLeadJet->pt() / 1000.);
        hist("lead_jet_eta_before_Calib_vs_after_Calib")->Fill(leadJet->eta(), calibLeadJet->eta());
        hist("lead_jet_phi_before_Calib_vs_after_Calib")->Fill(leadJet->phi(), calibLeadJet->phi());
      }
      if (subLeadJet && calibSubLeadJet)
      {
        hist("sub_lead_jet_pt_before_Calib_vs_after_Calib")->Fill(subLeadJet->pt() / 1000., calibSubLeadJet->pt() / 1000.);
        hist("sub_lead_jet_eta_before_Calib_vs_after_Calib")->Fill(subLeadJet->eta(), calibSubLeadJet->eta());
        hist("sub_lead_jet_phi_before_Calib_vs_after_Calib")->Fill(subLeadJet->phi(), calibSubLeadJet->phi());
      }



  
}



  return StatusCode::SUCCESS;
}

StatusCode AfpAnalysisExample ::finalize()
{

  xAOD::TEvent *event = wk()->xaodEvent();
  TFile *file = wk()->getOutputFile("skim");
  ANA_CHECK(event->finishWritingTo(file));

  //event_number_file.close();


  return StatusCode::SUCCESS;
}

// vim: expandtab tabstop=8 shiftwidth=2 softtabstop=2
